package org.rimasu.cli;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.rimasu.core.Thing;

/**
 * Entry point to application.
 */
public class Main {

    public static void main(String[] rawArgs) {
        CommandLineArgs args = parseArgsOrExit(rawArgs);

        System.out.println(String.format("Num repeats %d", args.getNumRepeats()));
        Thing thing = new Thing();
        for (int i=0;i<args.getNumRepeats();++i) {
            thing.doIt();
        }
    }

    private static CommandLineArgs parseArgsOrExit(String[] rawArgs) {
        CommandLineArgs args = new CommandLineArgs();
        CmdLineParser parser = new CmdLineParser(args);
        try {
            parser.parseArgument(rawArgs);
        } catch (CmdLineException e) {
            System.err.println(e.getMessage());
            // TODO read name from properties file
            System.err.println("java example-cli [options...] arguments...");
            parser.printUsage(System.err);
            System.err.println();
            System.exit(-1);
        }
        return args;
    }
}
