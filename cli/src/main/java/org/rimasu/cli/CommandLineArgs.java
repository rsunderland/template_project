package org.rimasu.cli;

import org.kohsuke.args4j.Option;

/**
 * Arguments that can be specified on command line.
 */
public class CommandLineArgs {

    @Option(name="-n",usage="repeat <n> times\n repeat the process a number of times")
    private int numRepeats = -1;

    public int getNumRepeats() {
        return numRepeats;
    }
}
