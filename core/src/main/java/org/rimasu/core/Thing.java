package org.rimasu.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Placeholder for something.
 */
public class Thing {

    private final static Logger logger = LoggerFactory.getLogger(Thing.class);

    private boolean happened;

    public void doIt() {
        logger.info("Doing my thing");
        happened = true;
    }

    public boolean hasItHappened() {
        return happened;
    }
}
