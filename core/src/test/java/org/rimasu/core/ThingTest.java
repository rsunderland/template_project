package org.rimasu.core;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class ThingTest {

    // class under test
    private Thing thing;

    @Test
    public void canCreateAThing() {
        givenThatThingCreated();
        whenDoItHappens();
        thenItReallyHappens();
    }

    private void givenThatThingCreated() {
        thing = new Thing();
    }

    private void whenDoItHappens() {
        thing.doIt();
    }

    private void thenItReallyHappens() {
        assertThat(thing.hasItHappened(), is(true));
    }
}
